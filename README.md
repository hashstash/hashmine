HASHMINE
Redmine Theme
---
HashMine is a theme for Redmine 2.4.x based on the gitmike theme for Redmine.

see gitmike at https://github.com/makotokw/redmine-theme-gitmike/

INSTALLATION
* Clone the repo in your redmine/public/theme/ folder
* Go to Redmine > Administration > Settings > Display section
* Select theme "HashMine" from the display

LICENSE
Licensed under GPL3